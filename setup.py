from setuptools import setup

setup(
    name='conversation_analyzer',
    packages=['conversation_analyzer'],
    include_package_data=True,
    install_requires=[
        'flask',
        'watson-developer-cloud'
    ],
)
